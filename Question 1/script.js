function makeList() {
    // Establish the array which acts as a data source for the list
    let listData = [
        "Kyrie Irving",
        "Valentino Rossi",
        "Micheal Jordan",
        "Lebron James",
        "Kobe Bryant", "Steph Curry",
        "Anthony Davies",
        "Cristiano Ronaldo",
        "Lionel Messi",
        "Kevin Durant"
    ],
        // Making a container element for the list
        listContainer = document.createElement('div'),
        // Making the list
        listElement = document.createElement('ul'),
        // Setting up a loop that goes through the items in listItems one at a time
        numberOfListItems = listData.length,
        listItem,
        i;

    // Adding it to the page
    document.getElementsByTagName('body')[0].appendChild(listContainer);
    listContainer.appendChild(listElement);

    for (i = 0; i < numberOfListItems; ++i) {
        // creating an item for each one
        listItem = document.createElement('li');

        // Adding the item text
        listItem.innerHTML = listData[i];

        // Adding listItem to the listElement
        listElement.appendChild(listItem);
    }
}

// function call
makeList();